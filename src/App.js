import { Routes, Route } from "react-router-dom";
import Home from "./components/Home";
import Edit from "./components/Edit";
import NotFound from "./components/NotFound";



function App() {
  return (
    <div>
      <Routes>
        <Route path="/" element={<Home />} />   
        <Route path="/edit/:index" element={<Edit />} />
        <Route path='*' element={<NotFound />} />

    
      </Routes>
      
    </div>
  );
}

export default App;
