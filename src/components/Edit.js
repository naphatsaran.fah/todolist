import styled from "styled-components";
import { Row, Col, FormGroup, Form, Button, InputGroup } from "react-bootstrap";
import { useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";


const Card = styled.div`
  margin-top: 120px;
  padding-top: 20px;
  padding: 50px;
  background-color: #6898;
  width: 100%;
  height: 100%;
`;

const Title = styled.h3`
  margin-bottom: 50px;
`;

const InfoErr = styled.div`
  color: red;
`;

const EditTopicErr = styled.div`
  color: red;
`;

const EditTextErr = styled.div`
  color: red;
`;

export default function Edit() {
  const { index } = useParams();

  const [editTopic, setEditTopic] = useState("");
  const [editText, setEditText] = useState("");

  const [infoErr, setInfoErr] = useState("");
  const [editTopicErr, setEditTopicErr] = useState("");
  const [editTextErr, setEditTextErr] = useState("");

  const navigate = useNavigate();
  //Change path to home page

  const handleSuccess = (e) => {
    e.preventDefault();

    if (!editTopic && !editText) {
      setEditTopicErr("pless fill the topic");
      setEditTextErr("pless fill the text");

    } else if (editTopic.length > 20 && editText.length >= 3) {
      setEditTopicErr("The length must be less than 20 characters.");
      setEditTextErr("");
      setInfoErr("");

    } else if (editText.length > 80 && editTopic.length >= 3) {
      setEditTopicErr("");
      setEditTextErr("The length must be less than 80 characters.");
      setInfoErr("");

    } else if (editTopic.length < 3 && !editText) {
      setEditTopicErr("topic  must contain at least 3 characters");
      setEditTextErr("");

    } else if (editTopic.length >= 3 && !editText) {
      setInfoErr("Please fill out the information completely.");
      setEditTopicErr("");
      setEditTextErr("");

    } else if (!editTopic || !editText) {
      setInfoErr("Please fill out the information completely.");

    } else if (editTopic.length < 3) {
      setEditTopicErr("topic  must contain at least 3 characters");

    } else if (editTopic.length >= 3 && editText.length < 3) {
      setEditTopicErr("");
      setEditTextErr("text  must contain at least 3 characters");
      setInfoErr("");

    } else if (editText.length < 3) {
      setEditTextErr("text  must contain at least 3 characters");

    } else if (editText.length >= 3 && editTopic.length < 3) {
      setEditTextErr("");
      setEditTopicErr("topic  must contain at least 3 characters");
 
    } else {

      let datas = JSON.parse(localStorage.getItem("datas"));
      datas[index] = { 
        addDate: datas[index].addDate,
        text: editText, 
        topic: editTopic ,
        updateDate: new Date()
      };
      localStorage.setItem("datas", JSON.stringify(datas));
      navigate(`/`);
    }
  };

  useEffect(() => {
    let datas = JSON.parse(localStorage.getItem("datas"));
    setEditText(datas[index] ? datas[index].text : "");
    setEditTopic(datas[index] ? datas[index].topic : "");
  }, []);


  return (
    <div>
      <Row className="justify-content-center">
        <Col sm="12" md="6" lg="6">
          <Card className="shadow-sm">
            <FormGroup className="text-center">
              <Title>Edit Todo List</Title>
            </FormGroup>

            <InfoErr>{infoErr}</InfoErr>
            <FormGroup className="mb-3" controlId="formBasicEmail">
              <Form.Control
                type="text"
                placeholder="Topic"
                value={editTopic}
                onChange={(e) => setEditTopic(e.target.value)}
              />
              <EditTopicErr>{editTopicErr}</EditTopicErr>
            </FormGroup>

            <br />
            <InputGroup className="mb-3">
              <InputGroup.Text>Detail</InputGroup.Text>
              <Form.Control
                as="textarea"
                aria-label="With textarea"
                value={editText}
                onChange={(e) => setEditText(e.target.value)}
              />
              <EditTextErr>{editTextErr}</EditTextErr>
            </InputGroup>
            <FormGroup className="d-grid">
              <Button variant="success" onClick={handleSuccess}>
                Success
              </Button>
            </FormGroup>
          </Card>
        </Col>
      </Row>
    </div>
  );
}
