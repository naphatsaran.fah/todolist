import styled from "styled-components";
import { Row, Col, FormGroup, Form, Button, InputGroup } from "react-bootstrap";
import { useState, useEffect } from "react";
import * as Icons from "react-icons/fa";
import { useNavigate } from "react-router-dom";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css"; // Import css confirmAlert
import moment from "moment";

const Card = styled.div`
  margin-top: 20px;
  display: block;
  width: 100%;
  padding: 10px;
  background: white;
  border-radius: 6px;

`;

const CardItem = styled.div`
  display: block;
  width: 100%;
  padding: 10px;
  background: #dedfde;
  border-radius: 6px;
  margin-bottom: 6px;
 

`;

const BtnMenu = styled.div`
  display: inline;
  width: 32px;
  padding: 4px 6px;
  color: white;
  border-radius: 4px;
  text-align: center;
  cursor: pointer;
`;

const TopicErr = styled.div`
  color: red;
`;

const TextErr = styled.div`
  color: red;
`;

const InfoErr = styled.div`
  color: red;
`;

const DateTimeeAdd = styled.div`
  margin-top: 10px;
  padding: 5px;
  background: #5abb78;
  border-radius: 4px;
`;

const DateTimeeEdit = styled.div`
  margin-top: 10px;
  padding: 5px;
  background: #7eace6;
  border-radius: 4px;
`;

export default function Home() {
  const [topic, setTopic] = useState("");
  const [text, setText] = useState("");
  const [datas, setDatas] = useState([]);

  const [topicErr, settopicErr] = useState("");
  const [textErr, setTextErr] = useState("");
  const [infoErr, setInfoErr] = useState("");

  useEffect(() => {
    setDatas(
      localStorage.getItem("datas")
        ? JSON.parse(localStorage.getItem("datas"))
        : []
    );
  }, []);
  //   console.log(localStorage.getItem("datas"))

  const handleSubmit = () => {
    if (!topic && !text) {
      settopicErr("pless fill the topic");
      setTextErr("pless fill the text");

    } else if (topic.length > 20 && text.length >= 3) {
      settopicErr("The length must be less than 20 characters.");
      setTextErr("");
      setInfoErr("");

    } else if (text.length > 80 && topic.length >= 3) {
      settopicErr("");
      setTextErr("The length must be less than 80 characters.");
      setInfoErr("");

    } else if (topic.length < 3 && !text) {
      settopicErr("topic  must contain at least 3 characters");
      setTextErr("");

    } else if (topic.length >= 3 && !text) {
      setInfoErr("Please fill out the information completely.");
      settopicErr("");
      setTextErr("");

    } else if (!topic || !text) {
      setInfoErr("Please fill out the information completely.");

    } else if (topic.length < 3) {
      settopicErr("topic  must contain at least 3 characters");

    } else if (topic.length >= 3 && text.length < 3) {
      settopicErr("");
      setTextErr("text  must contain at least 3 characters");
      setInfoErr("");

    } else if (text.length < 3) {
      setTextErr("text  must contain at least 3 characters");

    } else if (text.length >= 3 && topic.length < 3) {
      setTextErr("");
      settopicErr("topic  must contain at least 3 characters");

    } else {
      // let temp = [...datas, { text: text, topic: topic, }];
      let temp = [...datas, { text: text, topic: topic, addDate: new Date() }];
      localStorage.setItem("datas", JSON.stringify(temp));
      setDatas(JSON.parse(localStorage.getItem("datas")));
      setText("");
      setTopic("");
      settopicErr("");
      setTextErr("");
      setInfoErr("");
    }

    // console.log(addTime);s
    console.log(topic);
    console.log(text);
  };

  const CardData = ({ text, index, topic, addDate, updateDate }) => {
    const navigate = useNavigate();

    //edit
    const handleEditTodo = (e) => {
      e.preventDefault();
      // console.log("send Edit.");
      //path edit
      navigate(`/edit/${index}`);
    };

    //delete
    const handleDeleteTodo = (e) => {
      //ป้องกันเว็บรีเฟรช
      e.preventDefault();

      confirmAlert({
        title: "Are you sure Delete this?",

        message: "Topic : " + topic + " Deteil : " + text,
        buttons: [
          {
            label: "Yes",
            onClick: () => {
              datas.splice(index, 1);
              console.log(datas);

              setDatas([...datas]);
              localStorage.setItem("datas", JSON.stringify(datas));
              setDatas(JSON.parse(localStorage.getItem("datas")));
              setText("");
            },
          },
          {
            label: "No",
          },
        ],
      });
    };

    var addDates = moment(addDate).format("LLLL");
    var updateDates = updateDate ? moment(updateDate).format("LLLL") : "";

    //formatDateTime
    // let AddDateFormat = new Date(curTime).getFullYear() + '/' + new Date(curTime).getMonth() + '/' + new Date(curTime).getDate()
    // let AddTimeFormat = new Date(curTime).getHours() + ":" + new Date(curTime).getMinutes() + ":" + new Date(curTime).getSeconds();

    return (
      <CardItem className="shadow-sm">
        <Row>
          <Col sm="8">Topic : {topic}</Col>
          <Col sm="4" className="text-end"></Col>
        </Row>

        <Row>
          <Col sm="8">Detail : {text}</Col>
          <Col sm="4" className="text-end">
            <BtnMenu className="bg-success me-1" onClick={handleEditTodo}>
              <Icons.FaEdit size="14" />
            </BtnMenu>

            <BtnMenu className="bg-danger" onClick={handleDeleteTodo}>
              <Icons.FaTrashAlt size="14" />
            </BtnMenu>
          </Col>
        </Row>

        <DateTimeeAdd>
          <Row>
            <Col sm="8">Date Add : {addDates} </Col>
          </Row>
        </DateTimeeAdd>

        <DateTimeeEdit>
          <Row>
            <Col sm="8">Date Edit : {updateDates} </Col>
          </Row>
        </DateTimeeEdit>
      </CardItem>
    );
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col sm="12" md="6" lg="6">
          <Card className="shadow-sm">
            <FormGroup className="text-center">
              <h4>Todo List</h4>
            </FormGroup>

            <InfoErr>{infoErr}</InfoErr>
            <FormGroup className="mb-3" controlId="formBasicEmail">
              <Form.Control
                type="text"
                placeholder="Topic"
                value={topic}
                onChange={(e) => setTopic(e.target.value)}
              />
              <TopicErr>{topicErr}</TopicErr>
            </FormGroup>

            <InputGroup className="mb-3">
              <InputGroup.Text>Detail</InputGroup.Text>
              <Form.Control
                as="textarea"
                aria-label="With textarea"
                value={text}
                onChange={(e) => setText(e.target.value)}
              />
            </InputGroup>
            <TextErr>{textErr}</TextErr>

            <FormGroup className="d-grid">
              <Button variant="primary" onClick={() => handleSubmit()}>
                เพิ่ม
              </Button>
            </FormGroup>

            <FormGroup className="mt-2">
              {datas.map((item, index) => {
                return (
                  <CardData
                    text={item.text}
                    topic={item.topic}
                    addDate={item.addDate}
                    updateDate={item?.updateDate}
                    index={index}
                    key={index}
                  />
                );
              })}
            </FormGroup>
          </Card>
        </Col>
      </Row>
    </div>
  );
}
